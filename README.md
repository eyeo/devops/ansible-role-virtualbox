# VirtualBox Ansible Role

This is an [Ansible](https://docs.ansible.com/ansible/latest/) role for
managing the [VirtualBox](https://www.virtualbox.org/) software.
